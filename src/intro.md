---
subject: У пошуках Кроумівського ікстеншену для генерації QR кодів
tags: chrome qr codes heroic tales
---

Так як [пан jurgen обіцяє](https://jurgen.dreamwidth.org/128470.html)
промені поносу всім хто буде писати свої цінні думки про 73%, то я
напишу про свою улюблену тему: екосистему бовзера кроум.

Захотів найпростішого: ікстеншену, який би малював qr коди. Могозин
гоогла, натуралменте, має нічого, окрім жахіття.

Переглянувши їх декілька штук та трохи покепкувавши з кетайських,
довелося написати своє, але процес пошуку був достатньо смішний, так
що їм можна поділитися.

У цьому пості є опис мети, методів + огляд перших 3х інкстеншенів,
рекомендованих гооглом. У наступних постах будуть відгуки про інші
(чимось цікаві або феноменально ідіотські) розширення. Загалом,
могозин на тему qr їх має > 400 штук, тому оглядів вистачить на кілька
років.

## Трішки трівіа для тих, хто живе під скелею

QR поділяються на 40 версій. Кожну версію можна закодувати у декількох
режимах, декількох кодуваннях та з 4 рівнями виправленням помилок
(EC). Напр

* v1, режим numeric, ec L (7%) дає можливість закодувати 41 цифру.
* v1, byte, ec H (30%) дозволяє лише 7 байтів.
* v40, byte, ec L дозволяє більш комфортні 2953 байта, але qr код тоді
  повинен бути фізично розміром з телевізора, інакше телехфони, через
  жалюгідність своїх камер, будуть неспроможні його прочитати.

Для того щоб створювати суспільно корисні об'яви як то

<img src='https://henry-flower.dreamwidth.org/file/37179.png' alt='здогадайтеся'>

qr потрібно кодувати ув utf8, що ще більше зменшує його місткість.

Що я хотів від ікстеншену?

* ніяких `<all_urls>` та `https://*/*` дозволів, ніяких бекграундних
  сторінок, які висять постійно ув пам'яті, ніякого слідкування за
  тим, що я генерую;
* регулювання параметрів генерації; звичайно, має бути режим, де qr
  версію воно вибирає автоматично найменшу;
* svg;
* адаптивне вікно для qr з великим вмістом данних.

Що я отримав? (Риторичне питання.)

## Мéта

Кожен відгук містить рейтинг, який рахується з набіру хфакторів. Чому
щось подібне є відсутнім ув могозині гоогла, на жаль, є невідомим.

Клітерій                           Бали
--------------------------------  -----
Публічне ріпо                     1
Постійна бекграунд сторінка       -1
`<all_urls>` дозвіл               -2
Інжектування скрипта будь-куди    -2
Мініфікований код                 -1
Обфускаційний код                 -2
Стеження за користувачем          -4
Обробка помилок при генерації QR  1
Контекстне меню                   1
Не Московія                       1

Напр

~~~{.factors}
---
public_repo: false
persistent_bg_page: true
all_urls_permission: true
content_script_allover: true
minificated: true
obfusticated: false
user_tracking: false
qr_errors_handling: false
context_menu: true
country: Кетай (Гонконга)
---
~~~

Найкраще vs. найгірше:

<div style="display: flex">
  <%= `./ranking yaml < test/best.yaml` %><%= `./ranking yaml < test/worst.yaml` %>
</div>

Всі рожево-зелені таблички генеруються автомагічно з ямлу:

~~~
<%= File.read 'test/unacceptable.yaml' %>
~~~

Працює це так: я пишу ці пости ув маркдауні, який pandoc конвертує ув
фрагмент html'у, який читає [скрипта](https://gitlab.com/sigwait/chrome-qr-codes-heroic-tales/blob/master/ranking), що підраховує рейтинг та
заміює вузли `<pre class="factor">...</pre>` на таблички з вбудованим
css.

## Бібліотеки

У більшості переглянутих ікстеншенів використовується 2 найдебільніші
з найдебільніших бібліотек для генерації qr.

Колись, 1 японса написав бібліотечку qrcode-generator, яка
випльовувала лише png, не підтримувала версії qr > 4 та мала чималу
кількість помилок.

Потім 1 кореєць написав обгортку для зе бібліотеки, де він виправив 0
багів, але куди додав генерацію svg. Щоб показати свою зневагу до
бідного японси, вихідний код оригінальної бібліотечки він притулив ув
мініфікованому вигляді. Обгортка стала популярнішою за річ, яку вона
обгортала.

Тим часом японса, який з журбою дивився на корейця, тихесенько додав
до qrcode-generator'а підтримку всіх qr версій та генерацію svg,
ascii, html tables та чорта ув ступі. Це мало хто помітив, тому що всі
продовжували (а) користуватися обгорткою, (б) голосно бідкатися на її
недоліки. Згодом корейцю все набридло і свій проект він покинув, чого
не зробили численні його користувачі.

Паралельно з корейцем, 1 ірландець загорнув qrcode-generator'а ув
обгортку для jquery. Щоб посилити корисне правило "якщо бачиш jquery у
назві бібліотеки--не підходь на кілометр до", він додав 0 нових
хфункції до старої версії qrcode-generator'а та виправив 0
помилок. Обгортка стала популярнішою за річ, яку вона обгортала.

Всі події вище відбувалися 7-6 років тому. Сьогодні, у 2х врапперах
використовується код qrcode-generator, який автора врапперів не
оновлювали >7 років. За цей час японса довів свою бібліотеку до
робастного стану, але людям, які шукають 'qr code javascript', це є
невідомим. На гітхабі, по тегу qr, можна миттєво знайти кілька дуже
непоганих альтернативних імплементації з тисячами зірок, але це не має
жодного значення: чого немає на 1й сторінці гоогла, того не існує.

## Збір

Що розширень буде так багато, я, звичайно, не підозрював. Якщо набрати
ув інпуті могозину "qr", воно випльовує такий список, що здається його
можна скролити безкінечно. Коли воно, нарешті, зупинилося, я набрав ув
devtools:

~~~
> $$('div[aria-label="Add to Chrome"]').length
482
~~~

та закрив обличчя руками. 482--це є кількість розширень. У мене таке
відчуття, що їх пишуть студенти ув буткемпах, інакше таку кількість
однотипного мотлоху пояснити складно.

Скопіювати геть усі лінки на .crx можна надрукувавши:

    > copy($$('a[type=W]').map(v => v.href).join`\n`)

і завантажити:

    $ xargs -n1 ../fetch-webstore-ext < urls

`fetch-webstore-ext` це є [скрипта](https://gitlab.com/sigwait/chrome-qr-codes-heroic-tales/blob/master/fetch-webstore-ext), який завантажує 1 .crx,
виколупує з нього ім'я з версією та розпаковує ув директорію типу

    name-ver.id

| *Этимъ полукресломъ*
| *Мастеръ Гамбсъ*
| *начинаетъ новую партiю мебели*


## Quick_QR_Code_Generator-7.93.afpbjjgbdimpioenaedcjgkaigggcdpp

~~~{.factors}
---
public_repo: false
persistent_bg_page: true
all_urls_permission: false
content_script_allover: false
minificated: false
obfusticated: false
user_tracking: true
qr_errors_handling: false
context_menu: true
country: Кетай (Пекіна)
---
~~~

Нумеро уно ув могозині на тему qr (133,290 users).

Має гоогл аналітику ув бекграундній сторінці. Додатково записує коли
користувач клікає ув попапі на лінках, які ведуть до могозину.

У сторінці опції--iframe фасебооку (здогадайтеся навіщо).

Ув коді присутні цікаві коментарі на колбеках, які файряться коли
користувач хоче перегенерувати qr:

~~~
// won't send any private data, only for debug
// trackContent('click:'+href);
~~~

`trackContent()` хфункція відсилала б свій параметр (url сторінки, яку
користувач заінкодив) до бекграундної сторінки, звідки воно пливло б
до гоогл аналітики. Закоментовано мабуть тому, що якийсь інший кетайса
пообіцяв вибити автору зуби.

Яка гарантія на не відкоментування назад у наступній версії? Ніякої,
звичайно. Користувач помітить нічого.

Окрім трекінгу:

* паніка при спробі генерації qr з кількома кб вводу--починає жерти 1
  ядро cpu на ~94%, жодного сповіщення чому qr є відсутнім;
* qr тільки фіксованого розміру, який не залежить від розміру вьюпорту;
* не підтримує svg;
* jquery + bootstrap (коментувати не буду, DW мене зобанить за хейт
  спіч).

Зате є кнопка донейшену через paypal.

Питання від наївного гамериканця:

> 'Hello! Your extension was highly recommended and requested... Various
> State and Federal laws require us to ensure the privacy and security
> of information when used by students or with student data. Can you
> provide the permissions this extension requires as well as any
> information it collects?'

ггг

## The_QR_Code_Extension-0.3.oijdcdmnjjgnnhgljmhkjlablaejfeeb

~~~{.factors}
---
public_repo: false
persistent_bg_page: false
all_urls_permission: false
content_script_allover: false
minificated: true
obfusticated: false
user_tracking: true
qr_errors_handling: false
context_menu: false
country: Германія
---
~~~

Всі ріквести прямують до `the-qrcode-generator dot com`.

2,774,842 users (Ув Києві у вихідний день людей менше.)

При спробі генерації qr з кількома кб вводу показує галімат'ю. Про те
що сталася помилка, можна дізнатися тільки полізши інспектором до
попапу.

Тягне з собою angularjs 1, тому що як то ще можна зробити віконце
з 2 кнопками? Тільки за допомогою свіжого, як мамонт, фреймвоку.

## Easy_QR_Code-1.8.2.emlhjldmodkejoblhpbldpbddilijblk

~~~{.factors}
---
public_repo: false
persistent_bg_page: false
all_urls_permission: false
content_script_allover: false
minificated: true
obfusticated: false
user_tracking: true
qr_errors_handling: false
context_menu: false
country: Кетай (Шеньчженя)
---
~~~

Автор не забуває просити грошей, фітбеку та могозинних зірок. Як
бачимо, це приносить плоди ув вигляди 3го місця.

На сторінці опцій має гоогл аналітику.

Дозволяє трохи твікати qr генерацію, але, як нескладно здогадатися,
при спробі генерації qr з кількома кб вводу воно показує пустий div. У
чому справа, користувачу не повідомляється.

Під капотом є звичний jquery мотлох з обгорткою допотопної версії
qrcode-generator.

Я би такі розширення видаляв з могозини не тому що вони злі за своєю
суттю, а суто по запобіганню вивиху у людей щелеп від нудьги.
