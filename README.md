## Requirements

* ruby 2.3+
* node 10+
* `gem install nokogiri`
* `npm -g i json dreamwidth-js`
* `dnf install pandoc dos2unix unzip curl`

## Posting

~~~
$ $EDITOR src/a-new-post.md
$ make _out/post/a-new-post.posted
~~~

## License

MIT.
