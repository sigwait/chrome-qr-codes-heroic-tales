SHELL := bash -o pipefail
out := _out
post := $(out)/post

all: $(patsubst src/%.md, $(out)/%.html, $(wildcard src/*.md))

$(out)/%.html: src/%.md ranking
	$(mkdir)
	erb -T 1 -P $< | pandoc -t html5 -p --no-highlight | ./ranking > $@

$(post)/%.posted: $(out)/%.html
	$(mkdir)
	dreamwidth-js entry-post --security $(if $(private),private,public) -s "$(call yaml,subject)" -t "$(call yaml,tags)" < $<
	@touch $@

yaml = $(shell ./yaml $1 < src/$(notdir $(basename $<)).md)
mkdir = @mkdir -p $(dir $@)
.DELETE_ON_ERROR:
